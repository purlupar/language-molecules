// Language Molecules
// Copyright (c) David Diem
// Version: October 2022

// SCENE
const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;

let Removables = [];

let scene, camera, renderer, controls;

window.addEventListener( 'load', init );
window.addEventListener( 'resize', onWindowResize );

const verbs = {
    // 0proc, 1init, 2v, 3Asp, T, 4Fin, 5Force
    [["proc", "init", "v", "asp", "T"]] : ["strokes"],
    [["init", "proc"]] : ["ass"]
};

const nouns = {
    // 0N, 1n, 2Num, 3Det
    [["det"]] : ["der", "the", "den", "die", "einen"],    
    [["num"]] : [],
    [["N*","xyz", "level4"]] : ["boy", "dog", "Kuchen", "Apfel", "Salat"]
};

const modifiers = {
    [["mod"]] : ["sometimes", "kind", "fluffy"]
    // split up into modT, modN, etc later
//    [["modN"]] : []
};


function drawSphere(r, wres, hres, x, y, z, distance, color) {
    const geometry = new THREE.SphereGeometry( r, wres, hres);
//    const material = new THREE.MeshPhongMaterial( { color: 0x443333 } );    
//    const material = new THREE.MeshToonMaterial( { color: 0x443333 } );
    const material = new THREE.MeshStandardMaterial( { color: color, metalness: 1-(1/(distance)) } );

    const sphere = new THREE.Mesh( geometry, material );
    sphere.position.set( x, y, z );
//    sphere.rotation.x += 0.1;
    scene.add( sphere );
    Removables.push( sphere ); 
};

// todo: use theta 1/8/pi for horizontal links
function drawCylinder(ra, rb, h, wres, hres, x, y, z, index, color, rotation) {
    let oe = false;
    let ths = 0;
    let thl = 2*3.1415;
    const geometry = new THREE.CylinderGeometry( ra, rb, h, wres, hres, oe, ths, thl );
    const material = new THREE.MeshStandardMaterial( { color: color, metalness: 1-index*0.1} );
    const cylinder = new THREE.Mesh( geometry, material );
    cylinder.position.set( x, y, z );
    cylinder.rotateZ(rotation);
    scene.add( cylinder );
    Removables.push ( cylinder );
}


function drawText(label, w, h, p, x, y, z, fontsize, bgcolor) {
    const container = new ThreeMeshUI.Block( {
	width: w,
	height: h,
	padding: p,
	backgroundColor: new THREE.Color( bgcolor ),
	backgroundOpacity: 0.3,
	justifyContent: 'center',
	textAlign: 'center',
	fontFamily: "./assets/Roboto-msdf.json",
	fontTexture: "./assets/Roboto-msdf.png"
    } );

    container.position.set( x, y, z );
    container.rotation.x = 0;
    scene.add( container );
    Removables.push( container ); 

    const mytextbox =  new ThreeMeshUI.Text( {
	content: label,
	fontSize: fontsize
    } );
    container.add( mytextbox );
//    Removables.push( mytextbox );  // don't remove on reset (but do for container)
}


function drawMolecule(word, labels, category, x0, y0) {
    let color = 0xffffff;
    if (category == "n") {
	color = 0x333388;
    } else if (category == "v") {
	color = 0x883333;
    }
    else if (category == "mod") {
	color = 0x338833;
    }    
    let m = labels.length;
    for (let i = 0; i < m; i++) {
	drawSphere(0.2, 32, 32, x0, y0+i/2, 0, i/2+1, color);
//      drawCylinder(ra, rb, h, wres, hres, x, y, z, index, color)
	drawCylinder(0.03, 0.03, 0.48, 40, 40, x0, y0+i/2, 0, 3, color);
	drawText(labels[i], 0.2, 0.15, 0, x0, y0+i/2+0.1, 0.2, 0.08, color);
    }
    drawText(word, 0.4, 0.2, 0, x0, y0, 0.2, 0.12, 0x000000);    
}

// connector: shades = x in NPx



function analyze(input) {
    let x = 0; // actual the spine's x coordinate
    let y = 0;
    let inputs = input.split(" ");
    for (let w = 0; w < inputs.length; w++) {
	for ([key, value] of Object.entries(verbs)) {
	    if (value.includes(inputs[w])) {
		// got our verb
		// draw it
		let levellist = key.split(',');
		drawMolecule(inputs[w], levellist, "v", x, y);
		// remove it from input query

		inputs.splice(w, 1);

		break; // and resume with the remaining nominals
	    }
	}
    }
    x += 1; // move sideways
    let w = 0
    while (w < inputs.length) {
	for ([key, value] of Object.entries(nouns)) {
	    if (value.includes(inputs[w])) {
		// first nominal
		// draw it
		let levellist = key.split(',');
		drawMolecule(inputs[w], levellist, "n", x, y);
		// remove it from input query
		inputs.splice(w, 1);
		if (levellist[0] == ["N*"]) { // move sideways and go to floor
		    x += 1;
		    y = 0;
		} else { // build upwards
		    y += 1/2;
		}
	    } else  // force jump to next word if nothing was found
	    { w += 1;
	    }
	}
    }
    console.log("doing modifiers now:");
    console.log(inputs);
    while (w < inputs.length) {
	for ([key, value] of Object.entries(nouns)) {
	    if (value.includes(inputs[w])) {
		// first modifier
		// draw it
		let levellist = key.split(',');
		drawMolecule(inputs[w], levellist, "mod", x, y);
		// remove it from input query
		inputs.splice(w, 1);
		if (levellist[0] == ["N*"]) { // move sideways and go to floor
		    x += 1;
		    y = 0;
		} else { // build upwards
		    y += 1/2;
		}
	    } else  // force jump to next word if nothing was found
	    { w += 1;
	    }
	}
    }
    //              ra, rb, h, wres, hres, x, y, z, index, color, rotation)
    drawCylinder( 0.03, 0.03, 0.5, 10, 10, 0.5, 0, 0, 0, 0xffffff, 1.5707);

}

function clearScene() {
    console.log(Removables);
    if( Removables.length > 0 ) {
        Removables.forEach(function(v,i) {
            v.parent.remove(v);
        });
        Removables = null;
        Removables = [];
    }
    ThreeMeshUI.update();
    //    controls.update()
    renderer.render( scene, camera, controls );
    
}

function init() {

    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0x000000 );

//    var light = new THREE.DirectionalLight ( 0xffffff );
    //    light.position.set(8,8,0);
    var light = new THREE.PointLight ( 0xffffff );
    light.position.set(6,8,0);    
    var light2 = new THREE.PointLight ( 0xffffff );
    light2.position.set(8,8,0);
//    scene.add( light );    

//    Removables.push( light ); // dont remove this!

    camera = new THREE.PerspectiveCamera( 60, WIDTH / HEIGHT, 0.1, 100 );
    scene.add( camera );
    camera.add( light );    
    camera.add( light2 );
    renderer = new THREE.WebGLRenderer( {
	antialias: true
    } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( WIDTH, HEIGHT );
    
    document.body.appendChild( renderer.domElement );
    
    controls = new THREE.OrbitControls( camera, renderer.domElement );
    
    myDefaultView();
    analyze("the kind boy strokes the fluffy dog");
    renderer.setAnimationLoop( loop );
}


function myDefaultView() {
    let zoom = 1.5;
    camera.position.set( 1.4*zoom, 2.0*zoom, 2.8*zoom );
    controls.target.set( 0, 2, 0 );
    controls.update();
}

function inputSubmit() {
    clearScene();    
    let input = document.getElementById("inputId").value;
    console.log(input);
    analyze(input);
}

function showExample1() {
    clearScene();    
    analyze("the kind boy strokes the fluffy dog");
}


function onWindowResize() {
// handles resizing the renderer when the viewport is resized
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}


function loop() {

    ThreeMeshUI.update();
    //    controls.update()
    renderer.render( scene, camera, controls );

}
